-module(chat_sup).

-behaviour(supervisor).

-export([ start_link/0 ]).

-export([ init/1 ]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->
    Children = [
		{ chat_listener, {chat_listener, start_link, [] },
		  permanent, 2000, worker, [chat_listener] },
		{ chat_client_sup, { chat_client_sup, start_link, [] },
		  permanent, 2000, supervisor, [chat_client_sup] },
		{ chat_manager, { chat_manager, start_link, [] },
		  permanent, 2000, worker, [chat_manager ] }
	       ],
    Strategy = { one_for_one, 0, 1 },
    { ok, { Strategy, Children } }.
