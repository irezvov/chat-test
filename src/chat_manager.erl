-module(chat_manager).

-behaviour(gen_server).

% API
-export([ start_link/0, add_client/1, remove_client/1, remove_client/0, login/1,
	  broadcast/1, private/2, say/1]).

% gen_server callbacks
-export([ init/1, handle_call/3, handle_cast/2, handle_info/2,
	  terminate/2, code_change/3 ]).

-record(state, { table_name }).

-define(TABLE, ?MODULE).

%%%-----------
%%% API
%%%-----------

start_link() ->
    start_link(?TABLE).

start_link(Table) ->
    gen_server:start_link({ local, ?MODULE }, ?MODULE, [Table], []).

%% Start client process for socket Sock
add_client(Sock) ->
    gen_server:cast(?MODULE, { add, Sock }).

%% Remove caller process from chat
remove_client() ->
    remove_client(self()).

%% Remove client process from chat by Pid
remove_client(Pid) ->
    gen_server:cast(?MODULE, { remove, Pid }).

%% Register process with name Login
login(Login) ->
    gen_server:call(?MODULE, { login, Login, self() }).

%% Send message for all client
broadcast(What) ->
    gen_server:cast(?MODULE, { broadcast, What }).

%% Send private message for client with name Login
private(Login, What) ->
    gen_server:cast(?MODULE, { private, Login, What, self() }).

%% Say something to all in chat
say(What) ->
    gen_server:cast(?MODULE, { say, What, self() }).

%%%-----------
%%% Callbacks
%%%-----------

init([Table]) ->
    Tbl = ets:new(Table, [private, named_table]),
    { ok, #state{ table_name = Tbl } }.

handle_call({ login, Login, From }, _, #state{ table_name = Tbl } = State) ->
    case ets:member(Tbl, Login) of
	false -> 
	    ets:insert(Tbl, [{ Login, From }, { From, Login }]),
	    say_to_all(Tbl, { service, Login ++ " was joined" }),
	    { reply, ok , State };
	true ->
	    { reply, { error, "Already used" }, State }
    end.

handle_cast({ say, What, From }, #state{ table_name = Tbl } = State ) ->
    [{ _, Login }] = ets:lookup(Tbl, From),
    say_to_all(Tbl, Login ++ ": " ++ What),
    { noreply, State };

handle_cast({ private, Login, What, From }, #state{ table_name = Tbl } = State ) ->
    Res = ets:lookup(Tbl, Login),
    [{ _, FromLogin }] = ets:lookup(Tbl, From),
    case Res of
	[{ _, Pid }] when is_pid(Pid) -> 
	    chat_client:send(Pid, { private_from, FromLogin, What }),
	    chat_client:send(From, { private_to, Login, What });
	_ -> 
	    chat_client:send(From, { service, "No user with name " ++ Login })
    end,
    { noreply, State };

handle_cast({ add, Sock }, State) ->
    { ok, Pid } = chat_client_sup:start_child(Sock),
    gen_tcp:controlling_process(Sock, Pid),
    { noreply, State };

handle_cast({ remove, Pid }, #state{table_name = Tbl } = State) ->
    case ets:lookup(Tbl, Pid) of
	[{ _, Login }] ->
	    delete(Tbl, Login, Pid);
	_ -> 
	    ok
    end,
    { noreply, State };

handle_cast({ broadcast, What }, #state{ table_name = Tbl } = State) ->
    say_to_all(Tbl, What),
    { noreply, State }.

handle_info(_, State) ->
    { noreply, State }.

terminate(_Reason, _State) ->
    ok.

code_change(_Old, State, _Extra) ->
    { ok, State }.

%%%-----------
%%% Private
%%%-----------

say_to_all(Tbl, What) ->
    ets:foldl(fun({P, _}, _) when is_pid(P) ->
		      chat_client:send(P, What);
		 (_, _) -> ok end, ok, Tbl).

delete(Tbl, Login, Pid) ->
    ets:delete(Tbl, Login),
    ets:delete(Tbl, Pid),
    say_to_all(Tbl, {service, Login ++ " has been left" }).
