-module(chat_listener).

-behaviour(gen_server).

-export([ start_link/0, start_link/1 ]).

-export([ init/1, handle_call/3, handle_cast/2, handle_info/2,
	  terminate/2, code_change/3 ]).

-define(DEFAULT_PORT, 12345).

-record(state, { port, lsock }).

%%%-----------
%%% API
%%%-----------

start_link() ->
    start_link(?DEFAULT_PORT).

start_link(Port) ->
    gen_server:start_link({ local, ?MODULE }, ?MODULE, [Port], []).

%%%-----------
%%% Callbacks
%%%-----------

init([Port]) ->
    { ok, LSock } = gen_tcp:listen(Port, [{ active, true }]),
    { ok, #state{ port = Port, lsock = LSock }, 0 }.

handle_call(_, _From, State) ->
    { noreply, State }.

handle_cast(_, State) ->
    { noreply, State }.

%% accept connections and add it to chat
handle_info(timeout, #state{ lsock = LSock } = State) ->
    { ok, Sock } = gen_tcp:accept(LSock),
    % give right on socket for chat_manager process
    gen_tcp:controlling_process(Sock, whereis(chat_manager)),
    chat_manager:add_client(Sock),
    { noreply, State, 0 }.

terminate(_Reason, #state{ lsock = S }) ->
    gen_tcp:close(S),
    ok.

code_change(_Old, State, _Extra) ->
    { ok, State }.
