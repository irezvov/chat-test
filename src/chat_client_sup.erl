-module(chat_client_sup).

-behaviour(supervisor).

-export([ start_link/0, start_child/1 ]).

-export([ init/1 ]).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_child(Sock) ->
    supervisor:start_child(chat_client_sup, [Sock]).

init([]) ->
    Children = [
		{ chat_client, {chat_client, start_link, [] },
		  temporary, 2000, worker, [chat_client] }
	       ],
    Strategy = { simple_one_for_one, 0, 1 },
    { ok, { Strategy, Children } }.
