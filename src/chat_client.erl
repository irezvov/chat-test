-module(chat_client).

-behaviour(gen_server).

% API
-export([ send/2, start_link/1 ]).

% gen_server callbacks
-export([ init/1, handle_call/3, handle_cast/2, handle_info/2,
	  terminate/2, code_change/3 ]).

-record(state, { sock, login = undefined }).

%%%-----------
%%% API
%%%-----------

start_link(Sock) ->
    gen_server:start_link(?MODULE, [Sock], []).

%% Send something to client with pid To
send(To, What) ->
    gen_server:cast(To, { send, What }).

%%%-----------
%%% Callbacks
%%%-----------

init([Sock]) ->
    { ok, #state{ sock = Sock }, 0 }.

handle_call(_, _From, State) ->
    { noreply, State }.

%% handle different types of message
handle_cast({ send, { service, What } }, #state{ sock = S } = State) ->
    send_to_socket(S, What ++ "\n"),
    { noreply, State };

handle_cast({ send, { private_from, Login, What } }, #state{ sock = S } = State) ->
    send_to_socket(S, Login ++ " whispers: " ++ What),
    { noreply, State };

handle_cast({ send, { private_to, Login, What } }, #state{ sock = S } = State) ->
    send_to_socket(S, "To " ++ Login ++ ": " ++ What),
    { noreply, State };

handle_cast({ send, What }, #state{ sock = S } = State) ->
    send_to_socket(S, What),
    { noreply, State }.

%% handle message from socket without login
handle_info({tcp, S, RawData}, #state{ sock = S, login = undefined } = State) ->
    Login = lists:filter(fun($\n) -> false;
			    ($\r) -> false;
			    (_) -> true end, RawData),
    case chat_manager:login(Login) of
	ok -> 
	    { noreply, State#state{ login = Login } };
	{error, Reason} ->
	    send_to_socket(S, Reason),
	    { noreply, State }
    end;

%% handle messages from tcp after login
handle_info({tcp, S, RawData}, #state{ sock = S } = State) ->
    Msg = parse_msg(RawData),
    handle_message(Msg),
    { noreply, State };

%% handle close event from socket
handle_info({tcp_closed, S}, #state{ sock = S } = State) ->
    chat_manager:remove_client(self()),
    { stop, normal, State };

%% request for login through zero-timeout
handle_info(timeout, #state{ sock = S, login = undefined } = State) ->
    send_to_socket(S, "Enter login: "),
    { noreply, State }.

terminate(_Reason, #state{ sock = S } ) ->
    chat_manager:remove_client(),
    gen_tcp:close(S),
    ok.

code_change(_Old, State, _Extra) ->
    { ok, State }.

%%%-----------
%%% Private
%%%-----------

send_to_socket(Sock, Data) ->
    gen_tcp:send(Sock, Data).

parse_msg(Data) ->
    Toks = string:tokens(Data, ":"),
    case Toks of
	[ Msg ] ->
	    { public, Msg };
	[ Login | Rest ] ->
	    { private, Login, string:join(Rest, $\:) }
    end.

handle_message({ private, ToLogin, Msg }) ->
    chat_manager:private(ToLogin, Msg);
handle_message({ public, Msg }) ->
    chat_manager:say(Msg).
